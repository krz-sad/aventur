APP     = aventura

SRCDIR  = src
OBJDIR  = bin

SRCS    := $(shell find $(SRCDIR) -name '*.c')
SRCDIRS := $(shell find . -name '*.c' -exec dirname {} \; | uniq)
OBJS    := $(patsubst %.c,$(OBJDIR)/%.o,$(SRCS))

CFLAGS  = -Wall

all: $(APP)

$(APP) : buildrepo $(OBJS)
	$(CC) $(OBJS) -o $(OBJDIR)/$@

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) $(APP)

buildrepo:
	@$(call make-repo)

run:
	./$(OBJDIR)/$(APP)

define make-repo
   for dir in $(SRCDIRS); \
   do \
	mkdir -p $(OBJDIR)/$$dir; \
   done
endef


