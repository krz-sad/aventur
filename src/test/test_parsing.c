#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../model/element.h"
#include "../model/parser.h"

void data_parsing_execute_test() {

    FILE *left_lung_data = open_data_file("data/left_lung.adv");
    Element *left_lung = parse_element(left_lung_data, ROOT_LEVEL);

    element_print(left_lung);
    element_destroy(left_lung);
}