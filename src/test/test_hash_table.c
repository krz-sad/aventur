#include <stdio.h>
#include <stdlib.h>

#include "../type/hash_table.h"

typedef struct {
    char *name;
    float float1;
    float float2;
    float float3;
} Table_Struct;

Table_Struct* table_struct_create() {
    Table_Struct *s = (Table_Struct*) malloc(sizeof(Table_Struct));
    return s;
}

void hash_table_execute_test() {
    Hash_Table *hash_table = hash_table_create(3);

    Table_Struct *struct1 = table_struct_create();
    struct1->name = "devonian";
    struct1->float1 = 1.1;
    struct1->float2 = 1.2;
    struct1->float3 = 1.3;

    Table_Struct *struct2 = table_struct_create();
    struct2->name = "carboniferous";
    struct2->float1 = 2.1;
    struct2->float2 = 2.2;
    struct2->float3 = 2.3;

    Table_Struct *struct3 = table_struct_create();
    struct3->name = "permian";
    struct3->float1 = 3.1;
    struct3->float2 = 3.2;
    struct3->float3 = 3.3;

    printf("\n--- TESTE DE INSERCAO ---------------------------\n\n");

    hash_table_insert(hash_table, "devonian", struct1);
    hash_table_insert(hash_table, "carboniferous", struct2);
    hash_table_insert(hash_table, "permian", struct3);
    hash_table_print(hash_table);

    printf("\n--- TESTE DE BUSCA ------------------------------\n\n");

    Table_Struct *output = NULL;

    output = hash_table_search(hash_table, "devonian");
    printf("hash_table_search(hash_table, \"devonian\"): %s\n", output->name);
    output = hash_table_search(hash_table, "carboniferous");
    printf("hash_table_search(hash_table, \"carboniferous\"): %s\n", output->name);
    output = hash_table_search(hash_table, "permian");
    printf("hash_table_search(hash_table, \"permian\"): %s\n", output->name);

    printf("\n--- TESTE DE REMOCAO ----------------------------\n\n");

    hash_table_remove(hash_table, "carboniferous");
    printf("hash_table_remove(hash_table, \"carboniferous\")\n");
    hash_table_print(hash_table);

    hash_table_remove(hash_table, "permian");
    printf("\nhash_table_remove(hash_table, \"permian\")\n");
    hash_table_print(hash_table);


    hash_table_remove(hash_table, "devonian");
    printf("\nhash_table_remove(hash_table, \"devonian\")\n");
    hash_table_print(hash_table);

    hash_table_destroy(hash_table);
}
