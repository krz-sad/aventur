#include <stdio.h>

#include "../model/parser.h"
#include "../type/hash_table.h"


void exits_parsing_execute_test(Hash_Table *elements_in_game) {
    Element *elem = NULL;
    parse_exits("data/exits.adv", elements_in_game);

    elem = (Element*) hash_table_search(elements_in_game, "coração");
    printf("\nLista de saídas do coração: ");
    list_print(elem->detail.exits);

    elem = (Element*) hash_table_search(elements_in_game, "artéria pulmonar direita");
    printf("\nLista de saídas da artéria pulmonar direita: ");
    list_print(elem->detail.exits);

    elem = (Element*) hash_table_search(elements_in_game, "artéria pulmonar esquerda");
    printf("\nLista de saídas da artéria pulmonar esquerda: ");
    list_print(elem->detail.exits);

    elem = (Element*) hash_table_search(elements_in_game, "veia pulmonar direita");
    printf("\nLista de saídas da veia pulmonar direita: ");
    list_print(elem->detail.exits);

    elem = (Element*) hash_table_search(elements_in_game, "veia pulmonar esquerda");
    printf("\nLista de saídas da veia pulmonar esquerda: ");
    list_print(elem->detail.exits);

    elem = (Element*) hash_table_search(elements_in_game, "pulmão direito");
    printf("\nLista de saídas do pulmão direito: ");
    list_print(elem->detail.exits);

    elem = (Element*) hash_table_search(elements_in_game, "pulmão esquerdo");
    printf("\nLista de saídas do pulmão esquerdo: ");
    list_print(elem->detail.exits);
}
