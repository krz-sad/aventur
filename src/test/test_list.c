#include <stdio.h>
#include <stdlib.h>

#include "../type/list.h"

typedef struct {
    char *name;
    float float1;
    float float2;
    float float3;
} List_Struct;

List_Struct* list_struct_create() {
    List_Struct *s = (List_Struct*) malloc(sizeof(List_Struct));
    return s;
}

void list_execute_test() {
    List *list = list_create();

    List_Struct *struct1 = list_struct_create();
    struct1->name = "devonian";
    struct1->float1 = 1.1;
    struct1->float2 = 1.2;
    struct1->float3 = 1.3;

    List_Struct *struct2 = list_struct_create();
    struct2->name = "carboniferous";
    struct2->float1 = 2.1;
    struct2->float2 = 2.2;
    struct2->float3 = 2.3;

    List_Struct *struct3 = list_struct_create();
    struct3->name = "permian";
    struct3->float1 = 3.1;
    struct3->float2 = 3.2;
    struct3->float3 = 3.3;

    printf("\n--- TESTE DE INSERCAO ---------------------------\n\n");

    list_insert(list, "devonian", struct1);
    list_insert(list, "carboniferous", struct2);
    list_insert(list, "permian", struct3);
    list_print(list);

    printf("\n--- TESTE DE BUSCA ------------------------------\n\n");

    List_Struct *output = NULL;

    output = list_search(list, "devonian");
    printf("list_search(list, \"devonian\"): %s\n", output->name);
    output = list_search(list, "carboniferous");
    printf("list_search(list, \"carboniferous\"): %s\n", output->name);
    output = list_search(list, "permian");
    printf("list_search(list, \"permian\"): %s\n", output->name);

    printf("\n--- TESTE DE OLHADINHA --------------------------\n\n");
    output = list_peek(list);
    printf("list_peek(list): %s\n", output->name);

    printf("\n--- TESTE DE REMOCAO ----------------------------\n\n");

    list_remove(list, "carboniferous");
    printf("list_remove(list, \"carboniferous\")\n");
    list_print(list);

    list_remove(list, "permian");
    printf("list_remove(list, \"permian\")\n");
    list_print(list);

    list_remove(list, "devonian");
    printf("list_remove(list, \"devonian\")\n");
    list_print(list);

    list_destroy(list);
}
