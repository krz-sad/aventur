#pragma once

#include "../type/bool.h"
#include "../type/hash_table.h"
#include "../type/list.h"


/**
 * This union holds the actual value of an attribute
 *
 * string: only set if the attribute is a string
 * number: only set if the attribute is a number
 */
typedef union {
    char *string;
    float number;
} Attribute_Value;


/**
 * This struct has the fields necessary to
 * represent the attributes of all elements
 *
 * name: string referencing the attribute
 * is_string: tell string attributes from float ones
 * value: the actual value held by Attribute_Value
 */
typedef struct {
    char *name;
    Bool is_string;
    Attribute_Value value;
} Attribute;


/**
 * Note this union allows some kind of inheritance by
 * only having one or another field set, depending on
 * which datatype is under use. An external variable
 * must be set and used to control which field can be
 * accessed from within a Detail variable
 *
 * attributes: is only available to Object
 * exits: is only available to Place
 */
typedef union {
    Hash_Table *attributes;
    List *exits;
} Detail;


/**
 * This struct is parent to every element of the game. It
 * contains all the fields that can be present in any element
 *
 * detail: the union that holds attributes for regular objects an exits for places
 * is_active: tells if the element is active in the game
 * is_visible: tells if the element is visible to the player
 * is_known: tells if the element has already been described
 * is_place: tells places from regular objects
 * contents: list of contents the element has
 * actions: list of actions that only the element can execute
 * articles: singular and plural grammar articles
 * name: string to recognize the elements
 * desc_short: short description of the element
 * desc_long: long description of the element
 * animation: function to execute on every interaction with the element
 */
typedef struct {
    Detail detail;

    Bool is_active;
    Bool is_visible;
    Bool is_known;
    Bool is_place;

    List *contents;
    List *actions;
    char *articles[2];

    char *name;
    char *desc_short;
    char *desc_long;

    void *animation;
} Element;


Element* element_create_from_name(char *name, char *desc_short, char *desc_long, Bool is_place);
void element_destroy(Element *elem);
Attribute* element_get_attribute(Element *elem, char *name);
void* element_set_attribute(Element *elem, Bool is_string, char *key, char *value);
void element_print(void *elem);
void element_content_print(Element *elem);
void element_actions_print(Element *elem);
void element_exits_print(Element *elem);
void element_attributes_print(Element *elem);
