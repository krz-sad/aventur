#pragma once

#include <stdio.h>

#include "element.h"

#define ROOT_LEVEL 0

#define MARKER_INDENT      '\t'
#define MARKER_START_BLOCK '{'
#define MARKER_END_BLOCK   '}'
#define MARKER_NEXT_BLOCK  '\n'

#define MAX_LEN_ARTICLE         5
#define MAX_LEN_SMALL_AUX_STR   41
#define MAX_LEN_NAME            41
#define MAX_LEN_SHORT_DESC      71
#define MAX_LEN_LONG_DESC       121
#define MAX_LEN_BIG_AUX_STR     201

FILE* open_data_file(char *file_name);
Element* parse_element(FILE *data_file, int level);
void parse_exits(char *file_name, Hash_Table *elements_in_game);
