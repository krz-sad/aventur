#include <stdio.h>
#include <string.h>

#include "init/initializer.h"
#include "model/element.h"
#include "type/hash_table.h"
#include "type/list.h"

#define ELEMENTS_HASH_TABLE_LEN 37
#define ACTIONS_HASH_TABLE_LEN  13
#define MAX_COMMAND_LEN         30


/**
 * Reads a string from stdin an removes its trailling new-line
 * character. This function also makes a double check to ensure
 * we actually have new-line character at the end of the string
 * for safety reasons
 * 
 * @param string: the pointer to where we'll copy the string read
 */
void get_command(char *string) {
    fgets(string, MAX_COMMAND_LEN, stdin);
    short last_char_index = strlen(string) - 1;

    /**
     * Make sure we have the new-line char at the end of string
     */
    if (string[last_char_index] == '\n') {
        string[last_char_index] = '\0';
    }
}


void execute_action(Hash_Table *elements, Hash_Table *default_actions, char *str_object, char *str_place, char *action) {
    Element *character = NULL;
    Element *object = NULL;
    Element *place = NULL;
    Bool found_action = false;

    void (*action_function)(Element *elem);
    void (*generic_function)(void *character, void *object);

    character = hash_table_search(elements, "colônia de bactérias");
    object = hash_table_search(elements, str_object);
    place = hash_table_search(elements, str_place);

    /*
     * Attempts to execute the action with a function of the object passed as parameter
     */
    if (object) {
        action_function = list_search(object->actions, action);

        if (action_function) {
            found_action = true;
            action_function(object);
        }
    }
    if (!found_action && place) {
        action_function = list_search(place->actions, action);

        if (action_function) {
            found_action = true;
            action_function(place);
        }
    }

    if (!found_action) {
        generic_function = hash_table_search(default_actions, action);

        if (generic_function) {
            found_action = true;
            generic_function(character, object);
        }
    }

    if (!found_action) {
        printf("Acho esta ação não pode ser realizada pel%s %s nem pel%s %s...", object->articles[1], object->name, place->articles[1], place->name);
    }
}


int main() {
    Hash_Table *elements_in_game = NULL;
    Hash_Table *default_actions = NULL;
    
    Element *curr_place = NULL;
    Element *elem = NULL;

    char action[MAX_COMMAND_LEN];
    char subject[MAX_COMMAND_LEN];

    int  game_over = 0;

    /**
     * Initializes the game with it's main areas and objects,
     * inserting them all into a hash table
     */
    elements_in_game = hash_table_create(ELEMENTS_HASH_TABLE_LEN);
    set_game_initial_state(elements_in_game);

    /**
     * Initializes the actions available to all objects
     */
    default_actions = hash_table_create(ACTIONS_HASH_TABLE_LEN);
    load_default_actions(default_actions);

    /*
     * Sets the initial place where the main character is
     */
    curr_place = hash_table_search(elements_in_game, "pulmão direito");

    /*
     * Initial story
     */
    printf("\n\n");
    printf("  Esta é a sua história, quem você é, o que vai ser e precisa fazer. Você é\n");
    printf("  uma colônia de bactérias que deve estar procurando no ponto mais profundo\n");
    printf("  de seu núcleo não-organizado qualquer coisa que possa lhe ajudar a\n");
    printf("  descobrir onde você está. É um pulmão. O direito, para ser mais específico.\n");
    printf("  Está tudo muito confuso por agora, mas garanto que daqui para frente\n");
    printf("  acontecerão apenas coisas boas - desde que você cumpra com seus objetivos,\n");
    printf("  é claro.");

    printf("\n\n");
    printf("  Antes você estava sozinha, era uma viajante solitária em forma de esporo\n");
    printf("  perdida no ar. Agora é robusta, tem companhia, amigos e pode se considerar\n");
    printf("  uma colônia - e das boas! Conseguiu até um hospedeiro, mesmo que não tenha\n");
    printf("  sido bem uma escolha dele. Pobrezinho! Quem diria que aquele hospital para\n");
    printf("  onde levaram a avó já moribunda serviria de palco para o encontro de vocês\n");
    printf("  dois? Certamente ninguém. Mas o que importa é que agora você tem o que\n");
    printf("  fazer, não viverá mais no tédio.");

    printf("\n\n");
    printf("  Eu pareço saber tudo sobre você - e de fato sei -, mas daqui em diante me\n");
    printf("  limitarei a dar apenas algumas dicas sobre o que precisa ser feito. Aqui\n");
    printf("  vai a primeira: explore! O pulmão direito de um homem adulto pode ser muito\n");
    printf("  mais legal do que você possa imaginar, mas para conhecê-lo melhor é preciso\n");
    printf("  explorar sem medo. Pode ir fundo!\n");

    /*
     * Main loop of the game
     */
    while (!game_over) {

        printf("\n\n... novo turno .........................................................\n");

        printf("\nVocê está n%s %s\n", curr_place->articles[1], curr_place->name);
        element_print(curr_place);

        printf("\nVocê pode digitar um dos seguintes comandos para testar este mundo:");
        printf("\n > eu         (para ver o seu estado atual)");
        printf("\n > agir       (para executar uma ação própria do personagem)");
        printf("\n > selecionar (para selecionar um elemento e depois realizar uma ação)");
        printf("\n > ir         (para ir para algum lugar)");
        printf("\n > fim        (para encerrar o jogo)");
        printf("\n - ");
        get_command(action);

        game_over = strcmp(action, "fim") == 0;

        if (!game_over) {

            if (strcmp(action, "eu") == 0) {
                element_print(hash_table_search(elements_in_game, "colônia de bactérias"));
                printf("\n\nAperte ENTER para continuar...\n");
                getchar();
            }

            else if (strcmp(action, "selecionar") == 0) {
                printf("\nQual objeto você quer selecionar? (sugiro não digitar nada errado nesta fase) ");
                printf("\n - ");
                get_command(subject);
                elem = (Element*) hash_table_search(elements_in_game, subject);
                element_print(elem);

                printf("\n\nEscolha uma ação para executar.");
                printf("\nAlém das ações específicas de elementos e lugares, você pode usar as ações padrão:\n");
                printf("\n > analisar   (para ver detalhes de um elemento)");
                printf("\n > chutar     (para chutar algum elemento)");
                printf("\n > comer      (para comer algum elemento)");
                printf("\n > quebrar    (para quebrar algum elemento)");
                printf("\n - ");

                get_command(action);
                execute_action(elements_in_game, default_actions, subject, curr_place->name, action);

                printf("\n\nAperte ENTER para continuar...\n");
                getchar();
            }

            else if (strcmp(action, "agir") == 0) {
                printf("\nQual ação quer realizar? Atualmente o personagem tem essas ações:");
                printf("\n > dividir    (para aumentar o tamanho da colônia)");
                printf("\n - ");

                get_command(action);
                execute_action(elements_in_game, default_actions, "colônia de bactérias", curr_place->name, action);

                printf("\n\nAperte ENTER para continuar...\n");
                getchar();
            }

            else if (strcmp(action, "ir") == 0) {
                printf("\nQuer ir para onde? (sugiro não digitar nada errado nesta fase) ");
                printf("\n - ");
                get_command(subject);
                elem = (Element*) hash_table_search(elements_in_game, subject);
                curr_place = elem;
            }

            else {
                printf("\nEsta não é uma opção válida. Há um menu. Use-o.");
            }
        }
    }

    /**
     * Nothing is left to do, so we can destroy the hash table
     */
    hash_table_destroy(elements_in_game);
    return 0;
}
