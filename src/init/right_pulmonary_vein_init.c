#include <stdio.h>

#include "../model/element.h"
#include "../model/parser.h"


void right_pulmonary_vein_salut(void *elem) {
    printf("\nNão sou um cereal, mas sou a veia. \"A veia\" kkk... A veia pulmonar direita.");
}


void right_pulmonary_vein_tell_bad_joke(void *elem) {
    right_pulmonary_vein_salut(elem);
}


Element* right_pulmonary_vein_create() {
    FILE *data_file = open_data_file("data/right_pulmonary_vein.adv");
    Element *right_pulmonary_vein = parse_element(data_file, ROOT_LEVEL);

    list_insert(right_pulmonary_vein->actions, "cumprimentar", right_pulmonary_vein_salut);
    list_insert(right_pulmonary_vein->actions, "contar piada ruim", right_pulmonary_vein_tell_bad_joke);

    return right_pulmonary_vein;
}
