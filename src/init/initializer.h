#pragma once

#include "../model/element.h"


void set_game_initial_state(Hash_Table *elements_in_game);
void load_default_actions(Hash_Table *default_actions);

Element* character_create();
Element* heart_create();
Element* left_lung_create();
Element* left_pulmonary_artery_create();
Element* left_pulmonary_vein_create();
Element* right_lung_create();
Element* right_pulmonary_artery_create();
Element* right_pulmonary_vein_create();
