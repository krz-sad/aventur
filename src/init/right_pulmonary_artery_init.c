#include <stdio.h>

#include "../model/element.h"
#include "../model/parser.h"


void right_pulmonary_artery_salut(void *elem) {
    printf("\nMeu nome é Artéria. Nome de família: Pulmonar Direita.");
}


void right_pulmonary_artery_relax(void *elem) {
    printf("\nA artéria pulmonar direita está de boa. O sr. coração faz todo o trabalho.");
}


Element* right_pulmonary_artery_create() {
    FILE *data_file = open_data_file("data/right_pulmonary_artery.adv");
    Element *right_pulmonary_artery = parse_element(data_file, ROOT_LEVEL);

    list_insert(right_pulmonary_artery->actions, "cumprimentar", right_pulmonary_artery_salut);
    list_insert(right_pulmonary_artery->actions, "relaxar", right_pulmonary_artery_relax);

    return right_pulmonary_artery;
}
