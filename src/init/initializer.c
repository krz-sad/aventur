#include <stdio.h>

#include "default_actions.h"
#include "initializer.h"

#include "../model/parser.h"
#include "../type/hash_table.h"
#include "../type/list.h"


/**
 * A macro to avoid repeating code at the initialization
 * 
 * @param element: the variable to initialize
 * @param alias: the name under which the element is inserted into the table
 */
#define INIT_ELEMENT(element) {                                     \
    element = element##_create();                                   \
    hash_table_insert(elements_in_game, element->name, element);    \
    add_nested_elements_to_game(element, elements_in_game);         \
}


/**
 * A function that recursively adds an element and all of its nested children
 * into the game (represented here by a hash table). Note this function descends
 * until the deepmost level, always checking if the given element isn't already in game
 * 
 * @param parent_elem: a pointer to parent element, whose children will be analyzed
 * @param elements_in_game: the hash table representing the game
 */
void add_nested_elements_to_game(Element *parent_elem, Hash_Table *elements_in_game) {
    Node *node = parent_elem->contents->head;
    
    /**
     * Iterates through all children of the parent element
     */
    while (node != NULL) {

        /**
         * If this element is not in game yet, insert it. This is based on
         * the fact that each element has a unique name
         */
        if (hash_table_search(elements_in_game, node->key) == NULL) {
            hash_table_insert(elements_in_game, node->key, node->value);

            /**
             * Descends into this element children, so that we leave no element
             * from the game out of the hash table
             */
            add_nested_elements_to_game(node->value, elements_in_game);
        }
        node = node->next;
    }
} 


/**
 * Sets the game initial state, by loading in memory all of places and
 * objects, connecting them together, as long as initializing some
 * proper methods
 * 
 * @param elements_in_game: the hash table that will represent the game
 */
void set_game_initial_state(Hash_Table *elements_in_game) {
    Element *character;
    Element *heart;
    Element *left_lung;
    Element *left_pulmonary_artery;
    Element *left_pulmonary_vein;
    Element *right_lung;
    Element *right_pulmonary_artery;
    Element *right_pulmonary_vein;

    INIT_ELEMENT(character);
    INIT_ELEMENT(left_lung);
    INIT_ELEMENT(left_pulmonary_artery);
    INIT_ELEMENT(left_pulmonary_vein);
    INIT_ELEMENT(right_lung);
    INIT_ELEMENT(right_pulmonary_artery);
    INIT_ELEMENT(right_pulmonary_vein);
    INIT_ELEMENT(heart);

    parse_exits("data/exits.adv", elements_in_game);
}


/**
 * Initializes the default actions, which are a bunch of methods
 * that can be applied to any element in the game. Note these methods
 * act as fallback to eventual unimplemented, element-specific versions
 * of them
 * 
 * @param default_actions: a hash table in which the default action
 * will be stored
 */
void load_default_actions(Hash_Table *default_actions) {
    hash_table_insert(default_actions, "analisar",  analyze);
    hash_table_insert(default_actions, "chutar",    kick);
    hash_table_insert(default_actions, "comer",     eat);
    hash_table_insert(default_actions, "quebrar",   break_the_fucking);
}
