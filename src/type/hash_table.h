#pragma once

#include "list.h"


/**
 * This struct is a generic hash table that is able to hold
 * different kinds of items. It uses the "chaining" strategy
 * to deal with collisions. Its param array contains references
 * to linked lists that actually store the elements.
 *
 * capacity: maximum number of items
 * number_of_items: current number of items
 * array: array of pointers to List
 */
typedef struct {
    int capacity;
    int number_of_items;
    List **array;
} Hash_Table;


Hash_Table* hash_table_create(int capacity);
void hash_table_destroy(Hash_Table *hash_table);
void hash_table_print(Hash_Table *hash_table);
void* hash_table_insert(Hash_Table *hash_table, char *key, void *value);
void* hash_table_remove(Hash_Table *hash_table, char *key);
void* hash_table_search(Hash_Table *hash_table, char *key);
unsigned long hash_generate(unsigned char *string);
