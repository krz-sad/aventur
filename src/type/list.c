#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "../model/element.h"


/**
 * Dynamically instantiates a generic list that is able 
 * to hold different kinds of items.
 *
 * @return a pointer to a list
 */
List* list_create() {
    List *list = malloc(sizeof(list));
    
    if (list == NULL) {
        return NULL;
    }
    list->head = NULL;
    list->length = 0;

    return list;
}


/**
 * Destroys the whole list by removing each of its nodes.
 * Note this function does not remove each node's associated
 * value. Since the list didn't allocate the value when it was
 * inserted into here, the list can't disallocate it.
 * 
 * @param linked_list: list to be freed
 */
void list_destroy(List *linked_list) {
    if (linked_list != NULL) {
        Node *aux = linked_list->head;
        
        while (aux != NULL) {
            linked_list->head = aux->next;
            free(aux);
            aux = linked_list->head;
        }
        free(linked_list);
    }
}


/**
 * Inserts a value into the list. This function inserts
 * the element at the end of the list.
 * 
 * @param  linked_list: list to insert info into
 * @param  key: a unique key to represent this value
 * @param  value: what will be inserted into list
 * @return the field value of the node just created
 */
void* list_insert(List *list, char *key, void *value) {
    Node *new_node, *aux;

    if (list == NULL) {
        return NULL;
    }    
    new_node = (Node*) malloc(sizeof(Node));

    if (new_node == NULL) {
        return NULL;
    }
    new_node->key = key;
    new_node->value = value;
    new_node->next = NULL;

    /**
     * Makes the new node the head if none exists
     */
    if (list->head == NULL) {
        list->head = new_node;
    }
    else {
        aux = list->head;

        /**
         * Otherwise, keeps iterating over the
         * elements until the last one is found
         */
        while (aux->next != NULL) {
            aux = aux->next;
        }
        aux->next = new_node;
    }
    list->length++;
    return new_node->value;
}


/**
 * Searches the list for a value.
 * 
 * @param  linked_list: list to search in
 * @param  key: what will be searched for
 * @return a pointer to the key found
 */
void* list_search(List *linked_list, char *key) {
    Node *node;

    if (linked_list == NULL) {
        return NULL;
    }  
    
    node = linked_list->head;
    while (node != NULL) {

        if (strcmp(node->key, key) == 0) {
            return node->value;
        }
        node = node->next; 
    }
    return NULL;
}


/**
 * Removes a value from the list. Two different 
 * procedures are implemented: one to remove info
 * from the head of the list and another one to
 * remove info from the middle of the list.
 *
 * @param  linked_list: list to remove the value from
 * @param  key: what will be removed
 * @return a pointer to the info removed
 */
void* list_remove(List *linked_list, char *key) {
    Node *aux, *prev;
    
    if (linked_list == NULL) {
        return NULL;
    }
    aux = linked_list->head;
    prev = NULL;
    
    while (aux != NULL) {
        if (strcmp(aux->key, key) == 0) {
            /**
             * Removes from list's head
             */
            if (aux == linked_list->head) {
                linked_list->head = aux->next;
            }
            else {
                /**
                 * Removes from the middle of the list
                 */
                prev->next = aux->next;
            }
            linked_list->length--;

            return aux->value;
        }
        prev = aux;
        aux = aux->next; 
    }
    return NULL;
}


/**
 * Checks the value at the head of the list.
 *
 * @param  linked_list: list to peek
 * @return a pointer to the info at the head of the list
 */
void* list_peek(List *linked_list) {
    if (linked_list->head) {
        return linked_list->head->value;
    }
    else {
        return NULL;
    }
}


/**
 * Prints a list using each node's associated key
 *
 * @param linked_list: a pointer to the list to print
 */
void list_print(List *linked_list) {
    Node *aux = linked_list->head;

    printf("{\n");
    while (aux != NULL) {
        printf(".  key: %s\n", aux->key);
        aux = aux->next;
    }
    printf("}\n");
}
