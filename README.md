# Aventura

O text-game mais divertido de todos os tempos renasce das cinzas, dessa vez melhor, mais inteligente e no cenário mais assustador de todos: O INSTITUTO DE MATEMÁTICA E ESTÁTICA DA USP.

### Regras de confecção de código

Com o objetivo de facilitar a colaboração entre os membros da equipe, as seguintes regras de estilo de código foram desenvolvidas. Leve-as consigo em cada nova iteração ou meandro desse projeto, lembrando que um código padronizado é um código robusto. 

<img src="https://i.imgur.com/6h8vabR.png" />

1. O código deve estar em inglês.

2. Usar indentação de 4 espaços (**NÃO** usar tabs).

3. Exceto em contadores, dar preferência a nomes de funções e variáveis que contenham ao menos **3 (TRÊS)** caracteres. **NÃO** fazer uso de expressões tais como:
   ```c
      int k = l * 2 + p->x;
      char n = c_s(20);
   ```

4. Nomes de tipos de dados devem começar com caixa alta e possuir `_` entre as palavras, caso seja um nome composto. Dessa forma, fica fácil diferenciá-los dos tipos padrão do C. Ex:
   ```c
      Hash_Table
      Element
      Unindexed_Random_Access_Mickey_Board
   ```

5. O código deverá usar o estilo snake_case. Isto é, são aceitos nomes de variáveis e funções tais como:
   ```c
      int max_tam
      char *most_viewed_page
      void interact_with_object(char* object_name)
      Hash_Table
   ```

   Porém, **NÃO** são aceitos nomes como:
   ```c
      int canWalkThroughThisPlace(char *placeName)
      long time-until-next-loop()
      char AReallyBigNoisyUglyClassName
   ```

6. Nomes de funções devem ser sempre verbos, descrevendo ações, portanto. Se uma função retorna somente `true/false` (0/1), seu nome deve ser iniciado por `is_`, a menos que ela diga se algo __existe em algum contexto__, caso em que deve ser iniciada por `there_is`. Ex:
   ```c
      void hash_table_destroy_item(char* name)
      int is_character_alive(char* name)
      int there_is_any_movement()
   ```

   **NÃO** use nomes como:
   ```c
      int alive(char *name)
      double a_beautiful_chinese_panda(int[] something)
   ```

7. Funções que manipulem um tipo de dados específico, tal como listas ou pilhas, precisam ter o nome da lista explicitado antes do nome da ação executada pela função. Evitem-se nomes puros genéricos e pouco informativos, como `create`, `destroy` e `get`. Bons exemplos de nomes são:
   ```c
      int stack_create(int size)
      float list_peek(List *list)
      int hash_table_is_full(Hash_Table *table)
      Element hash_table_get_pointer(char* name)
   ```

8. Nomes de variáveis devem ser substantivos, indicando objetos, elementos, itens. **NÃO** usar verbos para nomear variáveis. Exemplos de nomes válidos de variáveis:
   ```c
      double avg_rate
      char* finish_words_the_caracter_cant_speak
      Element dull_cat
   ```
   
   Em contrapartida, **NÃO SÃO PERMITIDOS** os nomes a seguir:
   ```c
      Stack convert_stack
      List make_walking_bees
      Hash_Table *cant_destroy_this_one
   ```